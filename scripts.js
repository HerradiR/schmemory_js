const cards = document.querySelectorAll('.card');
const board = document.querySelector('.board');
const playerTurn = document.getElementById('player-turn');
const winPlayer = document.getElementById('win-player');
const countPlayer1DOM = document.getElementById('count-player-1');
const countPlayer2DOM = document.getElementById('count-player-2');
const modal = document.querySelector('.modal-bg');
const modalCloseBtn = document.querySelector('.modal-close');


let flippedCard, first, second, lockBoard; 
let winCounter, pickCount, playerActive, playerWinner, countPlayer1, countPlayer2;



setUp();


function setUp() {
    shuffle();
    cards.forEach(card => {
        card.classList.remove('flip');
        card.addEventListener('click', flipCard);
    });

    modalCloseBtn.addEventListener('click', () => {
        modal.classList.remove('bg-active');
    });
    resetBoard();

    flippedCard = false;
    lockBoard = false;
    winCounter = 0;
    pickCount = 0;
    countPlayer1 = 0;
    countPlayer2 = 0;
    playerActive = 1;
    playerWinner = 0;
    first, second;


    board.classList.add('player1Board');


    countPlayer1DOM.innerHTML = countPlayer1;
    countPlayer2DOM.innerHTML = countPlayer2;
}

function shuffle() {
    cards.forEach(card => {
        let randomPos = Math.floor(Math.random() * 12);
        card.style.order = randomPos;
    });
}

function resetBoard() {
    [flippedCard, lockBoard] = [false, false];
    [first, second] = [null, null];
}

function flipCard() {
    if (lockBoard) return;
    if (this === first) return;
    this.classList.add('flip');
    if (!flippedCard) {
        flippedCard = true;
        first = this;
    } else {
        flippedCard = false;
        second = this;
        pickCount++;
        verifyMatch();
    }
}

function verifyMatch(){
    if (first.dataset.image === second.dataset.image)
    {
        if(playerActive === 1){
            countPlayer1++;
            countPlayer1DOM.innerHTML = countPlayer1;
        } else {
            countPlayer2++;
            countPlayer2DOM.innerHTML = countPlayer2;
        }
        disableClickCards();
    } else {
        unflipCards();
    }
}


function disableClickCards() {
    first.removeEventListener('click', flipCard);
    second.removeEventListener('click', flipCard);
    winCounter++;
    //Win condition: 6 (12 cards, 6 pairs to find)
    if(winCounter >= 6){
        setDOMScores();
        modal.classList.add('bg-active');
        setUp();
    }
}

function unflipCards() {
    lockBoard = true;
    setTimeout(() => {
        first.classList.remove('flip');
        second.classList.remove('flip');

        playerActive = playerActive === 1 ? 2 : 1;
        changePlayer(playerActive);

        resetBoard();
    }, 1000);

}

function setDOMScores() {
    if (countPlayer1 > countPlayer2) winPlayer.innerHTML = `Player 1`;
    else if (countPlayer1 < countPlayer2) winPlayer.innerHTML = `Player 2`;
    else winPlayer.innerHTML = `No one`;
}

function changePlayer(player) {
    board.classList.add(`player${player}Board`);
    board.classList.remove(`player${player - 1}Board`);
    board.classList.remove(`player${player + 1}Board`);
    playerTurn.innerHTML = player === 1 ? ` < Player ${player}` : `Player ${player} > `;

}

cards.forEach(card => card.addEventListener('click', flipCard));