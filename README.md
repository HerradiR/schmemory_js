# README #

A simple memory card game made in Vanilla Javascript, HTML and CSS.

## What is this repository for? ##

* Application to Avito for the position of FrontEnd Developer.

## How do I get set up? ##

* Clone the repository.
* Open the index.html file on Chrome.

## What to add ? ##

* Possible usernames for players
* Keep scores in cookies for later (need to get more information about cookie storing)
* Reset button if the players want to redo the game
* Add levels and variations to the game (higher level, more cards etc...)

## What it looks like ? ##

![Capture](img/capture_img.PNG)